from django.shortcuts import render
from .models import Food

def food_list(request):
  foods = Food.objects.order_by('name')
  return render(request, 'macros/food_list.html', {'foods': foods})
