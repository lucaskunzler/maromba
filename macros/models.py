from django.conf import settings
from django.db import models

class Food(models.Model):
  name = models.CharField(max_length=200)
  description = models.TextField(null=True, blank=True)
  protein = models.DecimalField(max_digits=5, decimal_places=2)
  fat = models.DecimalField(max_digits=5, decimal_places=2)
  carbohydrate = models.DecimalField(max_digits=5, decimal_places=2)
  calories = models.DecimalField(max_digits=5, decimal_places=2)

  def __str__(self):
    return self.name
